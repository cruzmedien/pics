import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    Authorization:
      "Client-ID bcaab1d09f3e7e74cabd49ba1954d025b81a1eeec608290b73e1c3931d389701"
  }
});
